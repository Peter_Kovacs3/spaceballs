<%@ tag language="java" pageEncoding="ISO-8859-1" isELIgnored="false"  %>
<%@ attribute name="header" required="false" rtexprvalue="true" %>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Star Wars</a>
			</div>
			<div class="navbar-collapse collapse">
				<span>${loginError}</span>
				<form class="navbar-form navbar-right" role="form" action="/spaceballs/logout" method="post">
					<span class="white">Logged in as ${sessionScope.user.name}</span>
					<button type="submit" class="btn btn-success">Logout</button>
				</form>
			</div>
			<!--/.navbar-collapse -->
		</div>
	</div>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h1>${welcomeMessage}</h1>
			<p></p>
		</div>
	</div>
