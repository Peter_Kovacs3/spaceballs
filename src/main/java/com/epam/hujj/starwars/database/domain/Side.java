package com.epam.hujj.starwars.database.domain;

public enum Side {
    DARK("Dark side"),LIGHT("Light side")
    ;
    
    private String value;
    private Side(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
}
