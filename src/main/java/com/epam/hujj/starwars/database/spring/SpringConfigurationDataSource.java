package com.epam.hujj.starwars.database.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class SpringConfigurationDataSource {
	private static String dbUrl = "jdbc:mysql://localhost:3306/sql_excercise";
	private static String username = "root";
	private static String password = "root";

	
	@Bean
	DriverManagerDataSource dataSource() {
		return new DriverManagerDataSource(dbUrl, username, password);
	}
	
	
}
