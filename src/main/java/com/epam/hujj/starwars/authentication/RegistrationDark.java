package com.epam.hujj.starwars.authentication;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.epam.hujj.starwars.database.domain.Side;
import com.epam.hujj.starwars.database.domain.User;

public class RegistrationDark extends HttpServlet {
    private static final String HEADER_TEXT = "Join the dark side";
    private static final String DESCRIPTION = "The dark side of the Force, called Bogan or Boga by ancient Force-sensitives on Tython, was an aspect of the Force. Those who used the dark side were known as either Darksiders or Dark Jedi when unaffiliated with a dark side organization such as the Sith. Unlike the Jedi, who were famous for using the light side of the Force, darksiders drew power from raw emotions like anger, rage, hatred, fear, aggression, and passion. ";
    private static final List<String> MASTERS = Arrays.asList("Darth Sidous", "Darth Vader", "Darth Maul");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        setAttributes(request);
        final RequestDispatcher dispatcher = request.getRequestDispatcher("/registration.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        setAttributes(request);
        Map<String, String> parameters = getParameters(request);
        insertToDatabase(parameters, request);
        final RequestDispatcher dispatcher = request.getRequestDispatcher("/registration.jsp");
        dispatcher.forward(request, response);
    }

    private void setAttributes(HttpServletRequest request) {
        request.setAttribute("register", HEADER_TEXT);
        request.setAttribute("description", DESCRIPTION);
        request.setAttribute("masters", MASTERS);

    }

    private Map<String, String> getParameters(HttpServletRequest request) {
        Map<String, String> result = new HashMap<String, String>();
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String master = request.getParameter("master");
        String playerClass = request.getParameter("playerClass");
        result.put("name", name);
        result.put("password", password);
        result.put("master", master);
        result.put("playerClass", playerClass);
        return result;
    }

    private User createUser(Map<String, String> parameters) {
        User user = new User();
        user.setName(parameters.get("name"));
        user.setPassword(parameters.get("password"));
        user.setMaster(parameters.get("master"));
        user.setType(parameters.get("playerClass"));
        user.setLevel(1);
        user.setExperience(0);
        user.setSide(Side.DARK);
        return user;
    }

    private void insertToDatabase(Map<String, String> parameters, HttpServletRequest request) {
        SessionFactory sessionFactory = (SessionFactory) request.getServletContext().getAttribute("SessionFactory");
        Session session = sessionFactory.openSession();
        User user = createUser(parameters);
        boolean isExsistUser = isExsistUser(user, request);
        if (!isExsistUser) {
            Transaction tx = session.beginTransaction();
            session.persist(user);
            tx.commit();
            request.setAttribute("resultMessage", "You succesfully registered as a Sith");

        } else {
            request.setAttribute("resultMessage", "Name already in use please pick an other");
            request.setAttribute("params", parameters);
        }
    }

    private boolean isExsistUser(User user, HttpServletRequest request) {
        boolean result = false;
        SessionFactory sessionFactory = (SessionFactory) request.getServletContext().getAttribute("SessionFactory");
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<?> users = session.createCriteria(User.class).list();
        session.getTransaction().commit();
        if (!users.isEmpty()) {
            for (Object o : users) {
                User u = (User) o;
                if (u.getName().equals(user.getName())) {
                    result = true;
                }
            }
        }
        return result;

    }
}
