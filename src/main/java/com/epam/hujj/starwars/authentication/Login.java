package com.epam.hujj.starwars.authentication;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.epam.hujj.starwars.database.domain.User;

public class Login extends HttpServlet {
    private static final String REMEMBERME = "rememberme";
    private static final String USERS = "users";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String userName = request.getParameter("username");
        final String password = request.getParameter("loginPassword");

        User user  = getUser(userName, password, request);
       
        System.out.println("['" + userName + "','" + password + "']");
        if (user != null) {
            final HttpSession session = request.getSession();

            Cookie rememberme = findRememberme(request);
            Map<String, User> users = getUsers();

            if (rememberme == null) {
                UUID uuid = UUID.randomUUID();
                rememberme = new Cookie(REMEMBERME, uuid.toString());
                rememberme.setMaxAge(30 * 60);
                users.put(uuid.toString(), user);
            } else {
                String id = rememberme.getValue();
                //user = users.get(id);
            }

            response.addCookie(rememberme);

            synchronized (session) {
                session.setAttribute("user", user);
                session.setAttribute("loggedin", "true");
            }
            request.setAttribute("name", user.getName());
            String site = new String("/spaceballs/loggedin");
            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", site);
        } else {
            String site = new String("/spaceballs/hello");
            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            request.setAttribute("loginError", "Username/password incorrect");
            response.setHeader("Location", site);
            
        }
        

    }

    private Cookie findRememberme(final HttpServletRequest request) {
        Cookie result = null;
        for (Cookie c : request.getCookies()) {
            if (REMEMBERME.equals(c.getName())) {
                result = c;
                break;
            }
        }
        return result;
    }

    private Map<String, User> getUsers() {
        ServletContext context = getServletContext();
        Map<String, User> result = null;
        synchronized (context) {
            result = (Map<String, User>) context.getAttribute(USERS);
        }

        if (result == null) {
            result = new HashMap<>();

            synchronized (context) {
                context.setAttribute(USERS, result);
            }
        }
        return result;
    }

    private User getUser(String userName, String password, HttpServletRequest request) {
        User result = null;
        SessionFactory sessionFactory = (SessionFactory) request.getServletContext().getAttribute("SessionFactory");
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<?> users = session.createCriteria(User.class).list();
        session.getTransaction().commit();
        if (!users.isEmpty()) {
            for (Object o : users) {
                User u = (User) o;
                if (u.getName().equals(userName) && u.getPassword().equals(password)) {
                    result = u;
                }
            }
        }
        return result;
    }

}
