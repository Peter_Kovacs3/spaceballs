package com.epam.hujj.starwars.authentication;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoggedIn extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        final RequestDispatcher dispatcher = request.getRequestDispatcher("/loggedin.jsp");
        dispatcher.forward(request, response);
    }
    
}
