package com.epam.hujj.starwars;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Welcome extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("welcomeLight", "Join the light side");
        request.setAttribute("welcomeDark", "Join the dark side");
        request.setAttribute("welcomeMessage", "Welcome, Newbie!");
        request.setAttribute(
                "lightSideDescription",
                "The light side of the Force, also commonly known as the Ashla or simply the Force, was the side of the Force aligned with honesty, compassion, mercy, self-sacrifice, and other positive emotions. For the most part, the Jedi simply referred to this as the Force. ");
        request.setAttribute(
                "darkSideDescription",
                "The dark side of the Force, called Bogan or Boga by ancient Force-sensitives on Tython, was an aspect of the Force. Those who used the dark side were known as either Darksiders or Dark Jedi when unaffiliated with a dark side organization such as the Sith. Unlike the Jedi, who were famous for using the light side of the Force, darksiders drew power from raw emotions like anger, rage, hatred, fear, aggression, and passion. ");
        final RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);
    }
}
